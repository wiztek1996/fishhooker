﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGame : MonoBehaviour {


    public BigFishTarget bigFishTarget;
    public MediumFish mediumFish;
    public SmallFishTarget smallFishTarget;
    public BigRock bigRock;
    public Jellyfish jellyfish;
    public BoomTarget boomTarget;
    public TurtleScript turtleScript;
    public RandomItem randomItem;


    public Transform[] posCaNho;
    public Transform[] posCaTB;
    public Transform[] posCaTo;
    public Transform[] posRock;
    public Transform[] posJellyfish;
    public Transform[] posRandomItem;

    // Use this for initialization
    void Start () {
        GamePlayManager.isPause = false;
    }
}
