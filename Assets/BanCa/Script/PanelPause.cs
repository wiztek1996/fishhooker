﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelPause : MonoBehaviour {

    public GameObject panelPause, panelExit, panelHuongDan;
    public Button btnHome, btnHuongDan, btnTiepTuc;
    public Button btnDongY, btnHuy;
    public GamePlayManager gamePlayManager;
    // Use this for initialization
    private void OnEnable()
    {
        GamePlayManager.isPause = true;
        gamePlayManager.btnThuocNo.gameObject.SetActive(false);
    }
    private void Start()
    {
        btnHome.onClick.AddListener(GoToHome);
        btnHuongDan.onClick.AddListener(HuongDan);
        btnDongY.onClick.AddListener(Exit);
        btnHuy.onClick.AddListener(Huy);
        btnTiepTuc.onClick.AddListener(() => { panelPause.SetActive(false); }) ;
    }

    private void GoToHome()
    {
        panelExit.SetActive(true);
    }
    private void HuongDan()
    {
        panelHuongDan.SetActive(true);
    }
    public void ExitHuongDan()
    {
        panelHuongDan.SetActive(false);
    }
    private void Exit()
    {
        gamePlayManager.Exit();
    }
    private void Huy()
    {
        panelExit.SetActive(false);
    }
    private void OnDisable()
    {
        GamePlayManager.isPause = false;
        if (PlayerPrefs.GetInt(C.PP_SAVE_THUOCNO) > 0)
        {
            gamePlayManager.btnThuocNo.gameObject.SetActive(true);
        }
    }
    public void SetHide()
    {
        panelPause.SetActive(false);
    }
}
