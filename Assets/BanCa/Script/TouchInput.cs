﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TouchInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    
    public bool isTouchDown;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        isTouchDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isTouchDown = false;
    }
}
