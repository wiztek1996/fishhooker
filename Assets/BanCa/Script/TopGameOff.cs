﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopGameOff
{
    public int Stt { get; set; }
    public long UserID { get; set; }
    public string DisplayName { get; set; }
    public string Link_Avatar { get; set; }
    public long Score { get; set; }

    public TopGameOff(int stt, long userId, string displayName, string link_Avatar, long score)
    {
        this.Stt = stt;
        this.UserID = userId;
        this.DisplayName = displayName;
        this.Link_Avatar = link_Avatar;
        this.Score = score;
    }
}
