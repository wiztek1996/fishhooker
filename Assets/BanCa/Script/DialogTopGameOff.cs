﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTopGameOff : MonoBehaviour {

    private static DialogTopGameOff instance;
    public static DialogTopGameOff Instance { get { return instance; } }
    public Sprite[] arrTopNumber;
    public GameObject prefabTop;
    public Transform parentTop;
    // Use this for initialization
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != null)
            {
                Destroy(this.gameObject);
                return;
            }
        }
    }
    private void OnEnable()
    {
        CreateTopGame(B.Instance.lstTopGame);
    }

    public void CreateTopGame(List<TopGameOff> lstTopGame)
    {
        for(int i = 0; i < parentTop.childCount; i++)
        {
            Destroy(parentTop.GetChild(i).gameObject);
        }
        foreach(TopGameOff top in lstTopGame)
        {
            GameObject tmp = Instantiate(prefabTop);
            tmp.transform.SetParent(parentTop);
            tmp.transform.localPosition = Vector3.zero;
            tmp.transform.localScale = Vector3.one;
            tmp.GetComponent<ItemTopOff>().SetInfo(top);
        }
    }
}
