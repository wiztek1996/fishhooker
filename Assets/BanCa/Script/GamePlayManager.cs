﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayManager : MonoBehaviour
{
    private static GamePlayManager instance;
    public static GamePlayManager Instance { get { return instance; } }
    /// <summary>
    /// type : 1 thời gian, 2 vàng, 3 vật phẩm
    /// </summary>
    public int typeAds = 1;
    public float time = 60;
    public float _addTime = 0;
    public int totalMoney = 0;
    public int thuocNo = 0;
    public int moneyLevel;
    public int goldValue = 0;
    public int valueAddGold = 10;
    public int[] targetMoney;
    public int level = 1;
    public Text txtTotalMoney, txtTime, txtMoneyLevel, txtLevel, txtGold, txtNoti, txtHightScore;
    public Text txtMoneyMucTieu, txtLevelMucTieu, txtThongBao, txtThongBaoExit, txtThuocNo, txtMoneyFly, txtValueAddGold;
    public static bool isPause = false;
    public Button btnThoat, btnPause, btnExitLevel, btnAdsTime, btnQuaMan, btnThuocNo;
    public Button btnDongY, btnHuy;
    public Toggle tgAmThanh;
    public GameObject panelPause, panelWinGame, panelGameOver, panelMucTieu, panelThongBao, panelExitGame, flyMoney;
    GameObject levelObj;
    public LevelGame[] levelGames;
    public LevelGame levelTest;
    //public GoogleShowAds googleShowAds;
    public ShopItemOff shopItem;
    public Text txtTest;
    public TouchInput touchInput;
    private bool isAddTime = false;
    public Sprite[] iconsShop;

    public Transform[] posBomb;
    public GameObject bomb;
    public Transform parentBomb;

    public int valueAddFish = 0;
    public int valueAddRock = 0;
    public int valueAddSpeed = 0;
    public static bool isLucky = false;


    private void Awake()
    {
        // Load level chơi, bắt đầu mới hay load level cũ
        instance = this;
        Application.targetFrameRate = C.TARGET_FRAME;
        level = PlayerPrefs.GetInt(C.PP_SAVE_LEVEL);
        totalMoney = PlayerPrefs.GetInt(C.PP_SAVE_MONEY);
        if (level > 20 || level == 0)
        {
            level = 1;
        }
        if (level == 1)
        {
            thuocNo = 0;
            totalMoney = 0;
        }
        if (PlayerPrefs.GetInt(C.PP_GOLD_VALUE) == 0)
        {
            txtGold.text = goldValue + "";
        }
        else
        {
            goldValue = PlayerPrefs.GetInt(C.PP_GOLD_VALUE);
            txtGold.text = goldValue + "";
        }
        if (PlayerPrefs.GetInt(C.PP_SOUND, 1) == 1)
        {
            tgAmThanh.isOn = true;
        }
        else
        {
            tgAmThanh.isOn = false;
        }
        
        touchInput.gameObject.SetActive(true);
        txtTotalMoney.text = totalMoney + "";
        moneyLevel = targetMoney[level - 1];
        txtMoneyLevel.text = moneyLevel + "";
        txtMoneyMucTieu.text = moneyLevel + " $";
        txtLevelMucTieu.text = level + "";
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        CheckHightScoreLevel(level);
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(LoadLevelMap(level));
    }
    // Use this for initialization
    void Start()
    {
        txtTime.text = time + "";
        btnThoat.onClick.AddListener(ThoatGame);
        btnDongY.onClick.AddListener(Exit);
        btnHuy.onClick.AddListener(HuyThoat);
        btnPause.onClick.AddListener(PauseGame);
        btnExitLevel.onClick.AddListener(ExitLevel);
        btnAdsTime.onClick.AddListener(ShowAds);
        btnQuaMan.onClick.AddListener(QuaMan);
        SoundsOff.Instance.PlaySoundGame(1, true);
        tgAmThanh.onValueChanged.AddListener(delegate { AmThanh(); });
    }
    public void Test()
    {
        time -= 10;
        totalMoney += 500;
        txtTotalMoney.text = totalMoney + "";
    }
    private void SpawnBomb()
    {
        if (level == 5 || level == 10 || level == 15 || level == 20)
        {
            for (int i = 0; i < posBomb.Length; i++)
            {
                GameObject temp = Instantiate(bomb, posBomb[i].position, Quaternion.identity, parentBomb);
                BoomTarget boomTarget = temp.GetComponent<BoomTarget>();
                boomTarget.IsNo(false);
            }
        }
        else if (level == 3 || level == 8 || level == 13 || level == 18)
        {
            GameObject temp = Instantiate(bomb, posBomb[0].position, Quaternion.identity, parentBomb);
            BoomTarget boomTarget = temp.GetComponent<BoomTarget>();
            boomTarget.IsNo(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPause)
        {
            time -= Time.deltaTime;
            SetTime(time);
        }
    }
    public void SetMoney(int money)
    {
        SoundsOff.Instance.PlayMusicGame(4);
        txtTotalMoney.text = totalMoney + "";
    }
    int m = 0;
    private void SetTime(float time)
    {
        if (time < 10)
        {
            txtTime.color = Color.red;
            if (m == 0)
            {
                SoundsOff.Instance.PlayMusicGame(6);
                m++;
            }
        }
        else
        {
            txtTime.color = Color.white;
        }
        if (time < 1)
        {
            FinishGame();
        }
        txtTime.text = "" + (int)time;
    }
    
    private void AmThanh()
    {
        if (tgAmThanh.isOn)
        {
            SoundsOff.isMusic = true;
            PlayerPrefs.SetInt(C.PP_SOUND, 1);
            PlayerPrefs.Save();
            SoundsOff.Instance.aS.Play();
            SoundsOff.Instance.PlaySoundGame(1, true);
        }
        else
        {
            SoundsOff.isMusic = false;
            PlayerPrefs.SetInt(C.PP_SOUND, 0);
            PlayerPrefs.Save();
            SoundsOff.Instance.aS.Stop();
        }
    }
    public IEnumerator AddMoneyAndFly(GameObject obj, int money)
    {
        txtMoneyFly.text = "+ " + money + "$";
        totalMoney += money;
        SoundsOff.Instance.PlayMusicGame(3);
        if (money > 0)
        {
            flyMoney.SetActive(true);
        }
        if (obj != null)
        {
            // trừ boom còn lại phá hủy(boom xử lý chỗ khác)
            if (obj.name != "Boom(Clone)")
            {
                Destroy(obj);
            }
        }
        yield return new WaitForSeconds(1.2f);
        SetMoney(money);
        flyMoney.SetActive(false);
    }
    public void CheckHightScoreLevel(int level)
    {
        int tempScore = PlayerPrefs.GetInt("level" + level);
        B.Instance.hightScore = tempScore;
    }
    public IEnumerator LoadLevelMap(int level)
    {
        txtHightScore.text = B.Instance.hightScore + "";
        yield return new WaitForSeconds(0.1f);
        panelMucTieu.SetActive(true);
        yield return new WaitForSeconds(2f);
        panelMucTieu.SetActive(false);
        time = 60;
        time = time + _addTime;
        isAddTime = false;
        touchInput.gameObject.SetActive(true);
        levelObj = GameObject.Instantiate(levelGames[level - 1].gameObject);
        moneyLevel = targetMoney[level - 1];
        txtMoneyLevel.text = moneyLevel + "";
        txtLevel.text = level + "";
        thuocNo = PlayerPrefs.GetInt(C.PP_SAVE_THUOCNO);
        if (level == 1)
        {
            thuocNo = 0;
            totalMoney = 0;
        }
        if (thuocNo > 0)
        {
            btnThuocNo.gameObject.SetActive(true);
        }
        txtThuocNo.text = thuocNo + "";
        if (PlayerPrefs.GetInt(C.PP_GOLD_VALUE) == 0)
        {
            txtGold.text = goldValue + "";
        }
        else
        {
            goldValue = PlayerPrefs.GetInt(C.PP_GOLD_VALUE);
            txtGold.text = goldValue + "";
        }
        SpawnBomb();
    }


    // Sau khi check vượt ải thì cho sang level tiếp theo
    public void DestroyAndLoadMap(int newLevel)
    {
        StartCoroutine(DestroyAndLoadNewLevel(newLevel));
    }
    private IEnumerator DestroyAndLoadNewLevel(int levelNew)
    {
        shopItem.gameObject.SetActive(false);
        CheckHightScoreLevel(levelNew);
        yield return new WaitForSeconds(0.5f);
        moneyLevel = targetMoney[levelNew - 1];
        txtMoneyMucTieu.text = moneyLevel + " $";
        txtLevelMucTieu.text = levelNew + "";
        StartCoroutine(LoadLevelMap(levelNew));
    }
    private void PauseGame()
    {
        panelPause.SetActive(true);
        flyMoney.SetActive(false);
    }

    private void QuaMan()
    {
        if (goldValue >= 30)
        {
            Destroy(levelObj);
            goldValue -= 30;
            PlayerPrefs.SetInt(C.PP_GOLD_VALUE, goldValue);
            level += 1;
            PlayerPrefs.SetInt(C.PP_SAVE_LEVEL, level);
            PlayerPrefs.SetInt(C.PP_SAVE_THUOCNO, thuocNo);
            PlayerPrefs.SetInt(C.PP_SAVE_MONEY, totalMoney);
            PlayerPrefs.Save();
            panelGameOver.SetActive(false);
            shopItem.gameObject.SetActive(true);
        }
        else
        {
            if (MainMenu.Instance.IsLans())
            {
                SetThongBao("Bạn không đủ vàng");
            }
            else
            {
                SetThongBao("You do not have enough gold");
            }
        }

    }
    public void FinishGame()
    {
        //googleShowAds.ShowInterstitial();
        flyMoney.SetActive(false);
        _addTime = 0;
        valueAddFish = 0;
        valueAddRock = 0;
        valueAddSpeed = 0;
        isLucky = false;
        isPause = true;
        touchInput.gameObject.SetActive(false);
        btnThuocNo.gameObject.SetActive(false);
        CheckUpdateAndSaveScore(level, totalMoney);
        if (CheckLoadLevel())
        {
            Destroy(levelObj);
            level += 1;
            PlayerPrefs.SetInt(C.PP_SAVE_LEVEL, level);
            PlayerPrefs.SetInt(C.PP_GOLD_VALUE, goldValue);
            PlayerPrefs.SetInt(C.PP_SAVE_THUOCNO, thuocNo);
            PlayerPrefs.SetInt(C.PP_SAVE_MONEY, totalMoney);
            PlayerPrefs.Save();
            if (level <= 20)
            {
                panelWinGame.SetActive(true);
            }
            else
            {
                StartCoroutine(ChinhPhuc());
            }

        }
        else
        {
            if (isAddTime == false)
            {
                txtThongBaoExit.gameObject.SetActive(false);
                if (MainMenu.Instance.IsLans())
                {
                    txtThongBao.text = "Tiêu 30 vàng để qua màn hoặc xem quảng cáo để được thêm 10s ?";
                }
                else
                {
                    txtThongBao.text = "English ?";
                }
                isAddTime = true;
            }
            else
            {
                btnQuaMan.gameObject.SetActive(false);
                btnAdsTime.gameObject.SetActive(false);
                //txtThongBaoExit.transform.position = new Vector3(txtThongBaoExit.transform.position.x, txtThongBaoExit.transform.position.y-1f, txtThongBaoExit.transform.position.z);
                txtThongBaoExit.gameObject.SetActive(true);
                if (MainMenu.Instance.IsLans())
                {
                    txtThongBaoExit.text = "Rất tiếc bạn không hoàn thành mục tiêu";
                }
                else
                {
                    txtThongBaoExit.text = "English ?";
                }
            }
            panelGameOver.SetActive(true);
        }
    }
    public void CheckUpdateAndSaveScore(int level, int newScore)
    {
        // Check điểm level vừa chơi xong > level lưu trên máy mới gửi lệnh
        int tempScore = PlayerPrefs.GetInt("level" + level);
        if(newScore > tempScore)
        {
            PlayerPrefs.SetInt("level" + level, newScore);
            PlayerPrefs.Save();
        }
    }
    private bool CheckLoadLevel()
    {
        if (totalMoney >= moneyLevel)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private void ShowAds()
    {
        typeAds = 1;
#if UNITY_EDITOR
        //googleShowAds.Test();
#elif UNITY_ANDROID
        //googleShowAds.ShowRewardBasedVideo();
        Debug.Log("vào");
#elif UNITY_IPHONE
       //googleShowAds.ShowRewardBasedVideo();
#else
        //googleShowAds.ShowRewardBasedVideo();
#endif
    }
    private void ExitLevel()
    {
        StartCoroutine(GameOver());
    }
    public void ThoatGame()
    {
        panelExitGame.SetActive(true);
        isPause = true;
    }
    public void HuyThoat()
    {
        panelExitGame.SetActive(false);
        isPause = false;
    }
    public void Exit()
    {
        SceneManager.LoadScene(MoveScene.MAINMENU);
    }
    public IEnumerator ChinhPhuc()
    {
        level = 1;
        totalMoney = 0;
        thuocNo = 0;
        if (PlayerPrefs.GetString(C.PP_LANGUAGE) == "vi.json")
        {
            SetThongBao("Bạn đã chinh phục được biển cả!!!");
        }
        else
        {
            SetThongBao("You have conquered the sea !!!");
        }
        PlayerPrefs.SetInt(C.PP_SAVE_LEVEL, level);
        PlayerPrefs.SetInt(C.PP_SAVE_THUOCNO, thuocNo);
        PlayerPrefs.SetInt(C.PP_SAVE_MONEY, totalMoney);
        PlayerPrefs.Save();
        yield return new WaitForSeconds(1.5f);
        panelThongBao.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(MoveScene.MAINMENU);
    }
    private IEnumerator GameOver()
    {
        yield return new WaitForSeconds(0.5f);
        level = 1;
        totalMoney = 0;
        thuocNo = 0;
        //goldValue = 0;
        PlayerPrefs.SetInt(C.PP_SAVE_LEVEL, level);
        //PlayerPrefs.SetInt(C.PP_GOLD_VALUE, goldValue);
        PlayerPrefs.SetInt(C.PP_SAVE_THUOCNO, thuocNo);
        PlayerPrefs.SetInt(C.PP_SAVE_MONEY, totalMoney);
        PlayerPrefs.Save();
        yield return new WaitForSeconds(2);
        panelGameOver.SetActive(false);
        SceneManager.LoadScene(MoveScene.MAINMENU);
    }
    public void SetThongBao(string s)
    {
        panelThongBao.SetActive(true);
        txtNoti.text = s;
    }
    public void ClickClose()
    {
        panelThongBao.SetActive(false);
    }
    public IEnumerator AddItemAdsCallBack(int type, int values, GameObject obj)
    {
        Debug.Log("type :" + type);
        Debug.Log("values :" + values);
        yield return new WaitForSeconds(0.5f);
        if (type == 1)
        {
            SetThongBao("Bạn nhận được thêm " + values + " s");
            yield return new WaitForSeconds(1f);
            panelThongBao.SetActive(false);
            panelGameOver.SetActive(false);
            time = values;
            txtTime.text = time + "";
            isPause = false;
        }
        else if (type == 2)
        {
            valueAddGold = 20;
            txtValueAddGold.text = valueAddGold + "";
        }
        else if (type == 3)
        {
            if (obj != null)
            {
                SetThongBao("Bạn nhận được" +  " vật phẩm " + obj.name);
                AddItemVideo(obj);
            }
        }
    }
    public IEnumerator AddItemAdsCallBackObj(int type, int values)
    {
        Debug.Log("type 1 :" + type);
        Debug.Log("values 1 :" + values);
        yield return new WaitForSeconds(0.5f);
        if (type == 1)
        {
            SetThongBao("Bạn nhận được thêm " + values + " s");
            yield return new WaitForSeconds(1f);
            panelThongBao.SetActive(false);
            panelGameOver.SetActive(false);
            time = values;
            txtTime.text = time + "";
            isPause = false;
        }
        else if (type == 2)
        {
            SetThongBao("Bạn nhận được 20 vàng");
            yield return new WaitForSeconds(1f);
            panelThongBao.SetActive(false);
            valueAddGold = 20;
            txtValueAddGold.text = valueAddGold + "";
        }
    }
    public void AddItemVideo(GameObject obj)
    {
        if (obj.name == "Thuốc Nổ")
        {
            thuocNo += 1;
            PlayerPrefs.SetInt(C.PP_SAVE_THUOCNO, thuocNo);
            PlayerPrefs.Save();
        }
        else if (obj.name == "Thời Gian")
        {
            _addTime = 10;
        }
        else if (obj.name == "Sách")
        {
            valueAddRock = 10;
        }
        else if (obj.name == "Nước Tăng Lực")
        {
            valueAddSpeed = 10;
        }
        else if (obj.name == "Ngọc May Mắn")
        {
            isLucky = true;
        }
        else
        {
            valueAddFish = 10;
        }
        Debug.Log(obj.name);
        Destroy(obj);
    }
}
