﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config{
    public const string TAG_BIGFISH = "bigfish";
    public const string TAG_SMALLFISH = "smallfish";
    public const string TAG_MEDIUMFISH = "mediumfish";
    public const string TAG_RANDOM = "random";
    public const string TAG_BOOM = "boom";
    public const string TAG_DAYCAU = "daycau";
    public const string TAG_BIGROCK = "bigrock";
    public const string TAG_JELLYFISH = "jellyfish";
    public const string TAG_RUA = "rua";
}
