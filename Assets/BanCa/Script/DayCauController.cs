﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayCauController : MonoBehaviour
{

    public enum GameState
    {
        ROTATION,
        SHOOT,
        REWIND
    }
    public GameState gameState = GameState.ROTATION;

    private float _angle;
    private float _weight;

    private GamePlayManager gamePlayManager;
    public int _money;

    #region
#if UNITY_EDITOR
    [SerializeField]
    private static float _rotareSpeed = 1.2f;
#elif UNITY_ANDROID
    [SerializeField]
    private static float _rotareSpeed = 1.2f;
#elif UNITY_IPHONE
    [SerializeField]
    private static float _rotareSpeed = 1.5f;
#else
    [SerializeField]
    private static float _rotareSpeed = 1.5f;
#endif
    [SerializeField]
    private float _speed = 4.6f;
    [SerializeField]
    private Transform luoiCau;
    public Transform thuyenCau;
    public GameObject sung, dan;
    public Image bomb;
    Vector3 lineStart;
    Vector3 lineEnd;
    private LineRenderer lineRenderer;

    #endregion

    private Vector3 posBegin;
    private Transform _Item;
    private bool isKeo = false;


    public BigFishTarget bigFishTarget;
    public MediumFish mediumFish;
    public SmallFishTarget smallFishTarget;
    public BigRock bigRock;
    public Jellyfish jellyfish;
    public BoomTarget boomTarget;
    public TurtleScript turtleScript;
    public RandomItem randomItem;
    public GameObject player;
    Animator anima;
    public Animator animaDan;
    public bool isNo = false;
    private bool isHideDan = false;
    private bool isTamDung = false;


    private void Awake()
    {
        posBegin = transform.position;
    }

    private void Start()
    {
        gamePlayManager = FindObjectOfType<GamePlayManager>();
        lineStart = luoiCau.position;
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0, lineStart);
        lineRenderer.SetPosition(1, transform.position);
        gamePlayManager.btnThuocNo.onClick.AddListener(DestroyItem);
        anima = player.GetComponent<Animator>();
        _speed = _speed + ((_speed * gamePlayManager.valueAddSpeed) / 100);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (isKeo) return;
        isKeo = true;
        isHideDan = false;
        _Item = col.transform;
        if (col.tag == Config.TAG_BOOM)
        {
            isKeo = false;
            //Destroy(col.gameObject);
            _Item.tag = this.tag;
            boomTarget.IsNo(true);
            SoundsOff.Instance.PlayMusicGame(2);
            dan.SetActive(false);
            _weight = boomTarget.weight;
            _money = boomTarget.money;
        }
        else if (col.tag == Config.TAG_RANDOM)
        {
            _weight = randomItem.weight;
            _money = randomItem.money;
            randomItem.ItemRandom();
            //ItemRandom(randomItem.item, randomItem.valueItem);
            _Item.SetParent(transform);
            _Item.SetPositionAndRotation(transform.position, transform.rotation);
            Destroy(_Item.GetComponent<ItemTarget>());
        }
        else
        {
            if (col.tag == Config.TAG_BIGFISH)
            {
                _weight = bigFishTarget.weight;
                _money = bigFishTarget.money + ((bigFishTarget.money * gamePlayManager.valueAddFish) / 100);
            }
            else if (col.tag == Config.TAG_SMALLFISH)
            {
                _weight = smallFishTarget.weight;
                _money = smallFishTarget.money + ((smallFishTarget.money * gamePlayManager.valueAddFish) / 100);
            }
            else if (col.tag == Config.TAG_MEDIUMFISH)
            {
                _weight = mediumFish.weight;
                _money = mediumFish.money + ((mediumFish.money * gamePlayManager.valueAddFish) / 100);
            }

            else if (col.tag == Config.TAG_BIGROCK)
            {
                _weight = bigRock.weight;
                _money = bigRock.money + ((bigRock.money * gamePlayManager.valueAddRock) / 100);
            }
            else if (col.tag == Config.TAG_JELLYFISH)
            {
                _weight = jellyfish.weight;
                _money = jellyfish.money + ((jellyfish.money * gamePlayManager.valueAddFish) / 100);
            }
            else if (col.tag == Config.TAG_RUA)
            {
                _weight = turtleScript.weight;
                _money = turtleScript.money + ((turtleScript.money * gamePlayManager.valueAddFish) / 100);
            }
            _Item.SetParent(transform);
            _Item.SetPositionAndRotation(transform.position, transform.rotation);
            Destroy(_Item.GetComponent<ItemTarget>());
        }
        gameState = GameState.REWIND;
    }
    private void ItemRandom(string item, int value)
    {
        if (item == "Tiền")
        {
            StartCoroutine(gamePlayManager.AddMoneyAndFly(null, value));
        }
        else if (item == "Thuốc Nổ")
        {
            gamePlayManager.thuocNo += value;
            gamePlayManager.txtThuocNo.text = gamePlayManager.thuocNo + "";
            gamePlayManager.btnThuocNo.gameObject.SetActive(true);
        }
        else
        {
            _speed = 5.2f;
        }
    }

    public void DestroyItem()
    {
        if (!isTamDung)
        {
            StartCoroutine(DestroyItemTarget());
        }
    }
    /// <summary>
    /// Phá hủy 1 vật bằng thuốc nổ , Animation
    /// </summary>
    private IEnumerator DestroyItemTarget()
    {
        if (_Item != null)
        {
            if (gamePlayManager.thuocNo > 0)
            {
                isTamDung = true;
                // Ném 1 vật bay từ A-B
                yield return new WaitForSeconds(0.1f);
                animaDan.SetBool("SetNo", true);
                yield return new WaitForSeconds(0.8f);
                _weight = 0;
                gamePlayManager.thuocNo -= 1;
                gamePlayManager.txtThuocNo.text = gamePlayManager.thuocNo + "";
                Destroy(_Item.gameObject);
                if (gamePlayManager.thuocNo == 0)
                {
                    gamePlayManager.btnThuocNo.gameObject.SetActive(false);
                }
            }
        }
    }
    int click = 0;
    private void Update()
    {

        if (!GamePlayManager.isPause)
        {
            gamePlayManager.touchInput.gameObject.SetActive(true);
            switch (gameState)
            {
                case GameState.ROTATION:
                    // Nếu click chuột thì nhảy sang shot
                    click++;
                    if (click > 10)
                    {
                        if (gamePlayManager.touchInput.isTouchDown)
                        {
                            gameState = GameState.SHOOT;
                            SoundsOff.Instance.PlayMusicGame(7);
                            click = 0;
                        }
                    }
                    _angle += _rotareSpeed;
                    anima.SetBool("Fishing", true);
                    if(animaDan.gameObject.activeSelf)
                    {
                        animaDan.SetBool("SetNo", false);
                    }
                    isKeo = false;
                    if (_angle > 75 || _angle < -75)
                        _rotareSpeed *= -1;
                    // Vector3.forward = new Vector3(0, 0, 1);
                    transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
                    sung.transform.rotation = Quaternion.AngleAxis(_angle, Vector3.forward);
                    dan.SetActive(false);
                    break;
                case GameState.SHOOT:
                    // Đi xuống theo góc
                    transform.Translate(Vector3.down * _speed * Time.deltaTime);
                    anima.SetBool("Fishing", false);
                    if (animaDan.gameObject.activeSelf)
                    {
                        animaDan.SetBool("SetDan", false);
                    }
                    dan.SetActive(true);
                    //dan.SetActive(true);
                    // Đi ngược lại nếu ra khỏi màn hình or chạm vào vật phẩm
                    // Bị treo có thể do điều kiện này bị sai
                    if (Mathf.Abs(transform.position.x) > 7.5f || transform.position.y < -6.3f)
                    {
                        isKeo = true;
                        isHideDan = true;
                        dan.SetActive(false);
                        SoundsOff.Instance.PlayMusicGame(8);
                        gameState = GameState.REWIND;
                    }
                    break;
                case GameState.REWIND:
                    if (animaDan.gameObject.activeSelf)
                    {
                        animaDan.SetBool("SetDan", true);
                    }
                    if (!isTamDung)
                    {
                        transform.Translate(Vector3.up * (_speed - _weight) * Time.deltaTime);
                        if (isHideDan)
                        {
                            dan.SetActive(false);
                        }
                        else
                        {
                            dan.SetActive(true);
                            //lineRenderer.SetPosition(1, transform.position);
                        }
                    }
                    else
                    {
                        StartCoroutine(HideAnimation());
                    }
                    anima.SetBool("Fishing", false);
                    //Sounds.Instance.PlayMusicGame(5);
                    // Mathf.Floor lấy số nguyên nhưng sẽ lấy số bé hơn vd : 1.24 = 1, 1.95 = 1 (mục đích số float rất khó bằng nhau nên làm tròn xuống ntn cho dễ check điều kiện)
                    if (Mathf.Floor(transform.position.x) == Mathf.Floor(posBegin.x) && Mathf.Floor(transform.position.y) == Mathf.Floor(posBegin.y))
                    {
                        if (_Item != null)
                        {
                            isKeo = false;
                            _weight = 0;
                            // chèn hiệu ứng cộng tiền
                            StartCoroutine(gamePlayManager.AddMoneyAndFly(_Item.gameObject, _money));
                            if (_Item.gameObject.name == randomItem.name)
                            {
                                ItemRandom(randomItem.item, randomItem.valueItem);
                            }
                            //Destroy(_Item.gameObject);
                        }
                        gameState = GameState.ROTATION;
                        transform.position = posBegin;
                    }
                    break;
            }
        }
        else
        {
            gamePlayManager.touchInput.gameObject.SetActive(false);
            dan.SetActive(false);
        }
    }
    IEnumerator HideAnimation()
    {
        yield return new WaitForSeconds(1f);
        dan.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        isTamDung = false;
        yield return new WaitForSeconds(0.1f);
        isHideDan = true;
    }
}
