﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelWinGame : MonoBehaviour {

    public GamePlayManager gamePlayManager;
    public Button btnGoToShop;
    public Button btnX2Vang;
    public ShopItemOff shopItem;
    //public GoogleShowAds googleShowAds;

    private void OnEnable()
    {
        gamePlayManager.typeAds = 2;
        gamePlayManager.valueAddGold = 10;
        gamePlayManager.txtValueAddGold.text = gamePlayManager.valueAddGold + "";
    }
    private void Start()
    {
        btnGoToShop.onClick.AddListener(GoToShop);
        btnX2Vang.onClick.AddListener(X2Vang);
    }
   
    public void SetHide()
    {
        gameObject.SetActive(false);
    }
    private void X2Vang()
    {
#if UNITY_EDITOR
        //googleShowAds.Test();
#elif UNITY_ANDROID
        //googleShowAds.ShowRewardBasedVideo();
#elif UNITY_IPHONE
        //googleShowAds.ShowRewardBasedVideo();
#else
        //googleShowAds.ShowRewardBasedVideo();
#endif
    }
    private void GoToShop()
    {
        shopItem.gameObject.SetActive(true);
    }
}
