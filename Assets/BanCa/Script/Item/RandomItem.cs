﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomItem : ItemTarget {

    public string[] typeItem = { "Thuốc Nổ", "Tiền", "Thuốc Nổ", "Tiền", "Thuốc Nổ", "Tiền", "Thuốc Nổ", "Sức mạnh" };
    public string item;
    public int[] moneyItem = {11, 11, 11, 11, 11, 22, 22, 22, 22, 33, 33, 50, 100 };
    public int[] moneyLucky = {100, 200, 300, 400};
    public int valueItem = 0;
	public void ItemRandom()
    {
        int rand = Random.Range(0, 6);
        int randPoint = Random.Range(0, 12);
        int randLucky = Random.Range(0, 3);
        item = typeItem[rand];
        if(item == "Thuốc Nổ")
        {
            valueItem = 1;
        }
        else if(item == "Tiền")
        {
            if (!GamePlayManager.isLucky)
            {
                valueItem = moneyItem[randPoint];
            }
            else
            {
                valueItem = moneyLucky[randLucky];
            }
        }
        else
        {
            valueItem = 2;
        }
    }
}
