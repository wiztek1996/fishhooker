﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomTarget : ItemTarget
{

    Animator animBomb;
    
    public static bool isNo = false;
    // Use this for initialization

    private void Start()
    {
        animBomb = GetComponent<Animator>();
    }
    public void IsNo(bool isNos)
    {
        isNo = isNos;
    }
    private IEnumerator BomNo(Vector3 pos)
    {
        // 1 vị trí
        // 2 radius bán kính bằng 2
        // độ lệch khoảng cách dịch so vs vị trí ban đầu, zero tức ko dịch đi đâu cả
        var hits = Physics2D.CircleCastAll(pos, 1.8f, Vector2.zero);
        yield return new WaitForSeconds(0.9f);
        foreach (var hit in hits)
        {
            if (hit.collider == null) continue;
            if (hit.transform.tag == Config.TAG_BIGFISH || hit.transform.tag == Config.TAG_SMALLFISH ||
                hit.transform.tag == Config.TAG_MEDIUMFISH || hit.transform.tag == Config.TAG_RANDOM ||
                hit.transform.tag == Config.TAG_JELLYFISH || hit.transform.tag == Config.TAG_RUA ||
                hit.transform.tag == Config.TAG_BIGROCK)
            {
                Destroy(hit.transform.gameObject);
            }
            else
            {
                if (hit.transform.tag == Config.TAG_BOOM)
                {
                    hit.transform.tag = Config.TAG_BIGFISH;
                    StartCoroutine(hit.transform.GetComponent<BoomTarget>().BomNo(hit.point));
                }
            }
        }
    }
    private void Update()
    {
        if (isNo)
        {
            animBomb.SetBool("isNo", true);
            StartCoroutine(DestroyObj());
            StartCoroutine(BomNo(gameObject.transform.position));
        }
        else
        {
            animBomb.SetBool("isNo", false);
        }
    }
    private IEnumerator DestroyObj()
    {
        yield return new WaitForSeconds(0.9f);
        Destroy(gameObject);
    }
    private void DestroyItem(GameObject obj)
    {
        Destroy(obj);
    }
}
