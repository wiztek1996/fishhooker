﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTarget : MonoBehaviour {


    public string _tag;
    public int money;
    public float weight;

    public bool isBat = false;
    public Vector3 start;
    public Vector3 end;
    public Vector3 posStart;

    private void Awake()
    {
        this.tag = _tag;
    }
    // Use this for initialization
    void Start () {
        
	}
    public bool Check(Vector3 point)
    {
        return Mathf.Floor(point.x) == Mathf.Floor(transform.position.x);
    }
}
