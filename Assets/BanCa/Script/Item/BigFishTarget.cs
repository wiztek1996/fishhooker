﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigFishTarget : ItemTarget {
    
    

    [SerializeField]
    private float _speed;
    private void Start()
    {
        start = transform.position;
        start += Vector3.left * 3;
        end = transform.position;
        end += Vector3.right * 3;
    }

    void Update()
    {
        if (!GamePlayManager.isPause)
        {
            if (!isBat)
            {
                if (Check(start) || Check(end))
                {
                    _speed *= -1;
                }
                transform.Translate(Vector3.left * _speed * Time.deltaTime);
                if (Check(start))
                {
                    transform.localScale = new Vector3(-1f, 1f, 1f);
                }
                else if (Check(end))
                {
                    transform.localScale = new Vector3(1f, 1f, 1f);
                }
            }
        }

    }
    
}
