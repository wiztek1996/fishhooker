﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jellyfish : ItemTarget {

    [SerializeField]
    private float _speed;
    private void Start()
    {
        start = transform.position;
        start += Vector3.left * 6;
        end = transform.position;
        end += Vector3.right * 6;
    }

    void Update()
    {
        if (!GamePlayManager.isPause)
        {
            if (!isBat)
            {
                if (Check(start) || Check(end))
                {
                    _speed *= -1;
                }
                transform.Translate(Vector3.right * _speed * Time.deltaTime);
                if (Check(start))
                {
                    transform.localScale = new Vector3(1f, 1f, 1f);
                }
                else if (Check(end))
                {
                    transform.localScale = new Vector3(-1f, 1f, 1f);
                }
            }
        }

    }
}
