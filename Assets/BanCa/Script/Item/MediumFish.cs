﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumFish : ItemTarget
{
    
    private Animator anim;


    [SerializeField]
    private float _speed;
    private void Start()
    {
        anim = GetComponent<Animator>();
        start = transform.position;
        start += Vector3.left * 7;
        end = transform.position;
        end += Vector3.right * 7;
    }

    void Update()
    {
        if (!GamePlayManager.isPause)
        {
            if (!isBat)
            {
                if (Check(start) || Check(end))
                {
                    _speed *= -1;
                }
                //rigidbody2D.velocity = new Vector2(_speed, rigidbody2D.velocity.y);
                transform.Translate(Vector3.left * _speed * Time.deltaTime);
                if (Check(start))
                {
                    transform.localScale = new Vector3(-0.9f, 0.9f, 0.9f);
                }
                else if (Check(end))
                {
                    transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
                }
                //anim.SetFloat("Speed", Mathf.Abs(rigidbody2D.velocity.x));
            }
            anim.SetBool("isBat", isBat);
        }

    }

}
