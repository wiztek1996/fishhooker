﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemsShop : MonoBehaviour
{

    private GamePlayManager gamePlayManager;
    public string[] typeItem = { "Thuốc Nổ", "Thời Gian", "Mồi Câu", "Nước Tăng Lực", "Ngọc May Mắn", "Sách" };
    public Sprite[] icons;
    public Image icon;
    public Text txtTen, txtTien;
    public Button btnInfo, btnXem, btnMua;
    public Toggle tg;
    public GameObject groupBT;
    public int _thuocNo = 1;
    public int _timeBuy = 0;
    public int _priceBuy = 0;
    int m;

    // Use this for initialization
    private void Start()
    {
        gamePlayManager = FindObjectOfType<GamePlayManager>();
        btnMua.onClick.AddListener(BuyItem);
        //btnXem.onClick.AddListener(XemVideo);
        //btnInfo.onClick.AddListener(Test);
    }
    public void SetInfo(int i)
    {
        m = i;
        string t = typeItem[i];
        if (t == "Thuốc Nổ")
        {
            _priceBuy = 273;
            txtTien.text = _priceBuy + "$";
            icon.sprite = icons[i];
            this.gameObject.name = t;
        }
        else if (t == "Thời Gian")
        {
            _priceBuy = 400;
            txtTien.text = _priceBuy + "$";
            icon.sprite = icons[i];
            this.gameObject.name = t;
        }
        else if (t == "Sách")
        {
            _priceBuy = 22;
            txtTien.text = _priceBuy + "$";
            icon.sprite = icons[i];
            this.gameObject.name = t;
        }
        else if (t == "Nước Tăng Lực")
        {
            _priceBuy = 300;
            txtTien.text = _priceBuy + "$";
            icon.sprite = icons[i];
            this.gameObject.name = t;
        }
        else if (t == "Ngọc May Mắn")
        {
            _priceBuy = 295;
            txtTien.text = _priceBuy + "$";
            icon.sprite = icons[i];
            this.gameObject.name = t;
        }
        else
        {
            _priceBuy = 350;
            txtTien.text = _priceBuy + "$";
            icon.sprite = icons[i];
            this.gameObject.name = t;
        }
    }

    public void BuyItem()
    {
        Debug.Log("Cộng 1 vật phẩm " + this.gameObject.name);
        if (PlayerPrefs.GetInt(C.PP_SAVE_MONEY) >= _priceBuy)
        {
            if (this.gameObject.name == "Thuốc Nổ")
            {
                gamePlayManager.thuocNo += _thuocNo;
                gamePlayManager.totalMoney -= _priceBuy;
                PlayerPrefs.SetInt(C.PP_SAVE_THUOCNO, gamePlayManager.thuocNo);
                PlayerPrefs.Save();
            }
            else if (this.gameObject.name == "Thời Gian")
            {
                _timeBuy = 10;
                gamePlayManager._addTime = _timeBuy;
                gamePlayManager.totalMoney -= _priceBuy;
            }
            else if (this.gameObject.name == "Sách")
            {
                Debug.Log("Bạn được thêm 10 % giá trị của đá");
                gamePlayManager.totalMoney -= _priceBuy;
                gamePlayManager.valueAddRock = 10;
            }
            else if (this.gameObject.name == "Nước Tăng Lực")
            {
                Debug.Log("Bạn được tăng Sức mạnh");
                gamePlayManager.totalMoney -= _priceBuy;
                gamePlayManager.valueAddSpeed = 10;
            }
            else if (this.gameObject.name == "Ngọc May Mắn")
            {
                GamePlayManager.isLucky = true;
                gamePlayManager.totalMoney -= _priceBuy;
                Debug.Log("Cơ hộ nhận vật phẩm ngẫu nhiên giá trị");
            }
            else
            {
                Debug.Log("Tăng 10 % giá trị của cá");
                gamePlayManager.totalMoney -= _priceBuy;
                gamePlayManager.valueAddFish = 10;
            }
            PlayerPrefs.SetInt(C.PP_SAVE_MONEY, gamePlayManager.totalMoney);
            PlayerPrefs.Save();
            gamePlayManager.shopItem.txtMoney.text = gamePlayManager.totalMoney + "";
            gamePlayManager.txtTotalMoney.text = gamePlayManager.totalMoney + "";
            Destroy(this.gameObject);
        }
        else
        {
            gamePlayManager.SetThongBao("Bạn không đủ vàng để mua vật phẩm");
        }
    }
    public void XemVideo()
    {
#if UNITY_EDITOR
        //gamePlayManager.googleShowAds.objVatPham = this.gameObject;
        //gamePlayManager.googleShowAds.Test();
#elif UNITY_ANDROID
        //gamePlayManager.googleShowAds.objVatPham = this.gameObject;
        //gamePlayManager.googleShowAds.ShowRewardBasedVideo();
#elif UNITY_IPHONE
        //gamePlayManager.googleShowAds.objVatPham = this.gameObject;
        //gamePlayManager.googleShowAds.ShowRewardBasedVideo();
#else
        //gamePlayManager.googleShowAds.objVatPham = this.gameObject;
       // gamePlayManager.googleShowAds.ShowRewardBasedVideo();
#endif
        Debug.Log("Cộng 1 vật phẩm " + this.gameObject.name);
    }
}
