﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurtleScript : ItemTarget {

   
    [SerializeField]
    private float _speed;
    private void Start()
    {
        posStart = transform.position;
        start = transform.position;
        start += Vector3.left * 3;
        end = transform.position;
        end += Vector3.right * 3;
    }

    void Update()
    {
        if (!GamePlayManager.isPause)
        {
            if (!isBat)
            {
                if (CheckPos(start) || CheckPos(end))
                {
                    _speed *= -1;
                }
                transform.Translate(Vector3.left * _speed * Time.deltaTime);
                if (CheckPos(start))
                {
                    transform.localScale = new Vector3(-1f, 1f, 1f);
                }
                else if (CheckPos(end))
                {
                    transform.localScale = new Vector3(1f, 1f, 1f);
                }
            }
        }
    }
    bool CheckPos(Vector3 target)
    {
        return Mathf.Floor(target.x) == Mathf.Floor(transform.position.x);
    }

}
