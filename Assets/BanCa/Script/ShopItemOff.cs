﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemOff : MonoBehaviour
{

    public GamePlayManager gamePlayManager;
    public ToggleGroup toggleGroup;
    public Button btnNextLevel;
    public GameObject panelWinGame;
    public GameObject prefabsItem;
    public Transform parentItem;
    public GameObject Info;
    public Text txtInfo, txtMoney;
    public Image imgHang;

    public string[] item;

    // Use this for initialization
    void Start()
    {
        btnNextLevel.onClick.AddListener(GoToNextLevel);
    }
    private void OnEnable()
    {
        gamePlayManager.typeAds = 3;
        SpawnItem();
        Info.SetActive(false);
        panelWinGame.SetActive(false);
        txtMoney.text = PlayerPrefs.GetInt(C.PP_SAVE_MONEY) + " $";
    }

    public void GoToNextLevel()
    {

        gamePlayManager.goldValue += gamePlayManager.valueAddGold;
        gamePlayManager.txtGold.text = gamePlayManager.goldValue + "";
        PlayerPrefs.SetInt(C.PP_GOLD_VALUE, gamePlayManager.goldValue);
        PlayerPrefs.Save();
        gamePlayManager.DestroyAndLoadMap(gamePlayManager.level);
    }
    public void SpawnItem()
    {
        for (int i = 0; i < parentItem.childCount; i++)
        {
            Destroy(parentItem.GetChild(i).gameObject);
        }
        System.Random rand = new System.Random();
        string s = "";
        int temp = 0;
        int num = 6;
        List<int> list = new List<int>();
        for (int i = 0; i < num; i++)
        {
            list.Add(i);
        }

        for (int i = 0; i < 3; i++)
        {
            temp = rand.Next(list.Count);
            s += list[temp] + "|";
            list.RemoveAt(temp);
        }
        item = s.Split('|');
        for (int i = 0; i < 3; i++)
        {
            GameObject tmp = Instantiate(prefabsItem);
            int t = int.Parse(item[i]);
            tmp.transform.SetParent(parentItem);
            tmp.transform.localPosition = Vector3.zero;
            tmp.transform.localScale = Vector3.one;
            ItemsShop itemShop = tmp.GetComponent<ItemsShop>();
            itemShop.SetInfo(t);
            itemShop.tg.group = toggleGroup;
            itemShop.tg.onValueChanged.AddListener(delegate { SetInfo(t, itemShop); });
        }
    }
    private void SetInfo(int t, ItemsShop items)
    {
        Info.SetActive(true);
        imgHang.gameObject.SetActive(true);
        imgHang.sprite = items.icons[t];
        string lg = PlayerPrefs.GetString(C.PP_LANGUAGE);
        if(lg == "vi.json")
        {
            if (t == 0)
            {
                txtInfo.text = "Phá vật kéo lên không mong muốn";
            }
            else if (t == 1)
            {
                txtInfo.text = "Bạn sẽ được cộng thêm 10s vào màn sau, chỉ có giá trị trong 1 màn";
            }
            else if (t == 2)
            {
                txtInfo.text = "Tăng 10% giá trị của cá, chỉ có giá trị trong 1 màn";
            }
            else if (t == 3)
            {
                txtInfo.text = "Tăng 10% sức mạnh, chỉ có giá trị trong 1 màn";
            }
            else if (t == 4)
            {
                txtInfo.text = "Vật phẩm ngẫu nhiên sẽ có giá trị hơn, chỉ có giá trị trong 1 màn";
            }
            else
            {
                txtInfo.text = "Tăng 10% giá trị của vỏ ốc, chỉ có giá trị trong 1 màn";
            }
        }
        else
        {
            if (t == 0)
            {
                txtInfo.text = "After you have a grabbed onto something with you claw, touch dynamine area to throw a  dynamine at it and destroy it";
            }
            else if (t == 1)
            {
                txtInfo.text = "Give you an extra 10 seconds on the next";
            }
            else if (t == 2)
            {
                txtInfo.text = "Increases 10% of the value of the fish, used in 1 level";
            }
            else if (t == 3)
            {
                txtInfo.text = "Increases 10% of the value of the power, used in 1 level";
            }
            else if (t == 4)
            {
                txtInfo.text = "Increase the value when caching random iteam, used in 1 level";
            }
            else
            {
                txtInfo.text = "Increases 10% of the value of the conch, used in 1 level";
            }
        }
    }

}
