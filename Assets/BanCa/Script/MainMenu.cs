﻿//using Facebook.Unity;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    private static MainMenu instance;
    public static MainMenu Instance { get { return instance; } }
    public Button btnStart, btnVote, btnSetting, btnIAP, btnTop;
    public Image imgStart;
    public Sprite[] img;
    public SettingGameOff panelSetting;
    public GameObject panelIAP, panelTop;
    private string s = "vi.json";


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != null)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        if (PlayerPrefs.GetString(C.PP_LANGUAGE) == null || PlayerPrefs.GetString(C.PP_LANGUAGE) == "")
        {
            PlayerPrefs.SetString(C.PP_LANGUAGE, s);
            PlayerPrefs.Save();
        }
        SetLanguages();
        SoundsOff.Instance.PlaySoundGame(0, true);
    }

    // Use this for initialization
    void Start()
    {
        btnStart.onClick.AddListener(MoveScreen);
        btnSetting.onClick.AddListener(ShowSetting);
        btnIAP.onClick.AddListener(ShowIAP);
        btnTop.onClick.AddListener(ShowTop);
    }

    private void ShowSetting()
    {
        panelSetting.gameObject.SetActive(true);
    }
    private void ShowTop()
    {
        panelTop.gameObject.SetActive(true);
    }

    public void SetLanguages()
    {
        panelSetting.SetLanguages();
        if (IsLans())
        {
            imgStart.sprite = img[0];
        }
        else
        {
            imgStart.sprite = img[1];
        }
    }

    public bool IsLans()
    {
        string s = PlayerPrefs.GetString(C.PP_LANGUAGE);
        if (s == "vi.json")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public IEnumerator SetAvatar(Image img, string linkPicture)
    {
        if(linkPicture.Length > 2)
        {
            WWW www = new WWW(linkPicture);
            yield return www;
            if (www != null)
            {
                img.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
                img.gameObject.SetActive(true);
            }
        }
    }
    private void MoveScreen()
    {
        StartCoroutine(Login());
    }

    IEnumerator Login()
    {
#if UNITY_EDITOR

#else
        
#endif
        string token = "";
        if (PlayerPrefs.GetInt(C.PP_SAVE_ACCOUNT) == 1)
        {
            token = PlayerPrefs.GetString(C.PP_TOKEN_FB);
        }
        yield return new WaitForSeconds(0.1f);
        //if (FBlogin.Instance.isLogin)
        //{
        //    Debug.Log("fb");
        //    LoginController.Instance.Login(1, 1, token);
        //}
        //else
        //{
        //}
        yield return new WaitForSeconds(0.2f);
        SceneManager.LoadScene(MoveScene.GAME);
    }
   
    public void ShowIAP()
    {
        panelIAP.SetActive(true);
    }
    public void ClickBack()
    {
        panelIAP.SetActive(false);
    }
}
