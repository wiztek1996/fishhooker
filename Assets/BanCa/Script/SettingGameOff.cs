﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingGameOff : MonoBehaviour {


    public GameObject panelSetting, panelThongBao, panelHuongDan;
    public Text lbAmThanh, lbNhacNen, lbNgonNgu, txtTitle, txtContentHuongDan, txtTitleHuongDan, txtBtHuongDan, txtDangXuat, txtBtLogout;
    public Text txtBtDongY, txtBtHuy, txtTitleNoti ;
    public Button btnHuongDan, btnLogout;
    public Button btnOnSound, btnOffSound, btnDangXuat, btnDongY, btnHuy;
    public Dropdown drNgonNgu;
    public Toggle tgAmThanh, tgNhacNen;

    private void Awake()
    {
        if (PlayerPrefs.GetInt(C.PP_SOUND, 1) == 1)
        {
            tgAmThanh.isOn = true;
        }
        else
        {
            tgAmThanh.isOn = false;
        }
    }
    private void Start()
    {
        btnDangXuat.onClick.AddListener(() => { gameObject.SetActive(false); panelThongBao.SetActive(true); });
        btnDongY.onClick.AddListener(DongY);
        btnHuy.onClick.AddListener(Huy);
        btnHuongDan.onClick.AddListener(ShowGuide);
        tgAmThanh.onValueChanged.AddListener(delegate { AmThanh(); });
    }
    private void OnEnable()
    {
        if (MainMenu.Instance.IsLans())
        {
            drNgonNgu.value = 0;
        }
        else
        {
            drNgonNgu.value = 1;
        }
        //if (FBlogin.Instance.isLogin)
        //{
        //    btnLogout.gameObject.SetActive(true);
        //}
        //else
        //{
        //    btnLogout.gameObject.SetActive(false);
        //}
    }
    public void DrowDow_Change(int index)
    {
        if(index == 0)
        {
            StartCoroutine(LocatizationManager.instance.LoadLocalizedText("vi.json"));
        }
        else
        {
            StartCoroutine(LocatizationManager.instance.LoadLocalizedText("en.json"));
        }
    }
    private void AmThanh()
    {
        if (tgAmThanh.isOn)
        {
            SoundsOff.isMusic = true;
            PlayerPrefs.SetInt(C.PP_SOUND, 1);
            PlayerPrefs.Save();
            SoundsOff.Instance.aS.Play();
            SoundsOff.Instance.PlaySoundGame(0, true);
        }
        else
        {
            SoundsOff.isMusic = false;
            PlayerPrefs.SetInt(C.PP_SOUND, 0);
            PlayerPrefs.Save();
            SoundsOff.Instance.aS.Stop();
        }
    }
    public void ClickBack()
    {
        panelSetting.SetActive(false);
    }
    public void ClickExit()
    {
        panelHuongDan.SetActive(false);
    }
    private void DongY()
    {
        //FBlogin.Instance.FacebookLogout();
        panelThongBao.SetActive(false);
        panelSetting.SetActive(false);
        btnLogout.gameObject.SetActive(false);
    }
    void Huy()
    {
        panelThongBao.SetActive(false);
    }
     void ShowGuide()
    {
        panelHuongDan.SetActive(true);
        panelSetting.SetActive(false);
    }

    public void SetLanguages()
    {
        if (MainMenu.Instance.IsLans())
        {
            txtTitle.text = "Cài Đặt";
            //lbAmThanh.text = "Âm thanh :";
            //lbNhacNen.text = "Nhạc nền :";
            //lbNgonNgu.text = "Ngôn ngữ :";
            //txtBtHuongDan.text = "Hướng Dẫn";
            txtBtLogout.text = "Đăng Xuất";
            txtDangXuat.text = "Bạn muốn đăng xuất không ?";
            txtBtDongY.text = "Đồng Ý";
            txtBtHuy.text = "Hủy";
            txtTitleNoti.text = "Thông Báo";
            txtTitleHuongDan.text = "Hướng Dẫn";
        }
        else
        {
            txtTitle.text = "Setting";
            //lbAmThanh.text = "Music :";
            //lbNhacNen.text = "Sounds :";
            //lbNgonNgu.text = "Languages :";
            //txtBtHuongDan.text = "Guide";
            txtBtLogout.text = "Logout";
            txtDangXuat.text = "Do you want logout ?";
            txtBtDongY.text = "Yes";
            txtBtHuy.text = "No";
            txtTitleNoti.text = "Notification";
            txtTitleHuongDan.text = "Tutorial";
        }
    }
}
