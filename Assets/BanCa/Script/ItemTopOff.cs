﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemTopOff : MonoBehaviour {
    
    public Text txtName, txtLevel, txtStt, txtScore;
    public Image imgAvatar, imgStt;
    public TopGameOff topGame;

    // Use this for initialization
    void Start () {
		
	}
	
	public void SetInfo(TopGameOff _topGame)
    {
        this.topGame = _topGame;
        if(topGame.Stt < 4)
        {
            int index = topGame.Stt - 1;
            imgStt.sprite = DialogTopGameOff.Instance.arrTopNumber[index];
            imgStt.gameObject.SetActive(true);
            txtStt.gameObject.SetActive(false);
        }
        else
        {
            txtStt.text = topGame.Stt + "";
            imgStt.gameObject.SetActive(false);
            txtStt.gameObject.SetActive(true);
        }
        StartCoroutine(MainMenu.Instance.SetAvatar(imgAvatar, topGame.Link_Avatar));
        txtName.text = topGame.DisplayName;
        txtLevel.text = PlayerPrefs.GetInt(C.PP_SAVE_LEVEL) + "";
        txtScore.text = topGame.Score+ "";
    }
}
