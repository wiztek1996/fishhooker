﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;

public class LocatizationManager : MonoBehaviour {

    public static LocatizationManager instance;

    private bool isReady = false;
    private string missingText = "Ngôn ngữ";
    public GameObject panel;
    public Text txtThongBao;
    LocalizationData localizationData;

    private Dictionary<string, string> localizedText;
    // Use this for initialization
	private void Awake () {
		if(instance == null)
        {
            instance = this;
        }else if(instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        string s = PlayerPrefs.GetString(C.PP_LANGUAGE);
        StartCoroutine(LoadLocalizedText(s));
	}
	
	
    public IEnumerator LoadLocalizedText(string fileName)
    {
        localizedText = new Dictionary<string, string>();
        string filePath = "";
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            filePath = Path.Combine(Application.streamingAssetsPath + "/", fileName);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            filePath = "jar:file://" + Application.dataPath + "!/assets" + "/" + fileName;
        }
        else
        {
            filePath = Path.Combine(Application.streamingAssetsPath + "/", fileName);
        }
        string jsonString;
        if (filePath.Contains("://") || filePath.Contains(":///"))
        {
            Debug.Log("UNITY:" + System.Environment.NewLine + filePath);
            UnityWebRequest www = UnityWebRequest.Get(filePath);
            yield return www.SendWebRequest();
            jsonString = www.downloadHandler.text;
            jsonString = Encoding.UTF8.GetString(www.downloadHandler.data, 3, www.downloadHandler.data.Length - 3);
        }
        else
        {
            jsonString = File.ReadAllText(filePath);
        }
        
        localizationData = JsonUtility.FromJson<LocalizationData>(jsonString);
        for (int i = 0; i < localizationData.items.Length; i++)
        {
            localizedText.Add(localizationData.items[i].key, localizationData.items[i].value);
        }

        PlayerPrefs.SetString(C.PP_LANGUAGE, fileName);
        PlayerPrefs.Save();
        MainMenu.Instance.SetLanguages();

        isReady = true;
    }
    
    public string GetLocalizedValue(string key)
    {
        string result = missingText;
        if(localizedText != null)
        {
            if (localizedText.ContainsKey(key))
            {
                result = localizedText[key];
            }
        }
        return result;
    }
    private void SetThongBao(string s)
    {
        panel.SetActive(true);
        txtThongBao.text = s;
    }

    public bool GetIsReady()
    {
        return isReady;
    }
    public void ClickClose()
    {
        panel.SetActive(false);
    }
}
