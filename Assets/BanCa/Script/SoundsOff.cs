﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsOff : MonoBehaviour {

    public static SoundsOff Instance;
    public static bool isMusic = true;
    public AudioSource aS;
    public AudioClip[] aClip;

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        if (PlayerPrefs.GetInt(C.PP_SOUND, 1) == 1)
        {
            isMusic = true;
        }
        else
        {
            isMusic = false;
        }
        aS.volume = PlayerPrefs.GetFloat(C.PP_MUSIC, 0.1F);
    }

    public void PlayMusicGame(int music)
    {
        if (isMusic)
        {
            aS.PlayOneShot(aClip[music]);
        }
    }

    public void PlaySoundGame(int music, bool isLoop)
    {
        if (isMusic)
        {
            aS.clip = aClip[music];
            aS.Play();
            aS.loop = isLoop;
        }
    }
    
    /// <summary>
    /// Tắt âm thanh đang phát
    /// </summary>
    public void StopAudio(AudioSource audioSource)
    {
        audioSource.Stop();
    }

    public void Slide_Value_Music(float i)
    {
        aS.volume = i;
    }
}
