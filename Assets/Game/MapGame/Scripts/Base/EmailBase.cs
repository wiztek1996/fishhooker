﻿public class EmailBase : Base
{
    public int Id { get; set; }
    public string GuiTu { get; set; }
    public string GuiLuc { get; set; }
    public bool IsOpen { get; set; }

    public EmailBase(int id, string guiTu, string guiLuc, string content, bool isOpen)
    {
        this.Id = id;
        this.GuiTu = guiTu;
        this.GuiLuc = guiLuc;
        this.Content = content;
        this.IsOpen = isOpen;
    }
}
