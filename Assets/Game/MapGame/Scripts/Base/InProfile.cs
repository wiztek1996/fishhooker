﻿public class InProfile
{
    public string Name;
    public string Money;
    public int Type;
    public string DisplayName;
    public bool IsDaMoi = false;

    public InProfile(string name, string displayName, string money, int type, bool isDaMoi)
    {
        this.Name = name;
        this.DisplayName = displayName;
        this.Money = money;
        this.Type = type;
        this.IsDaMoi = isDaMoi;
    }
}
