﻿public class ItemLogBase
{
    public int GameId { get; set; }
    public string GameName { get; set; }
    public int Win { get; set; }
    public int Lose { get; set; }
}
