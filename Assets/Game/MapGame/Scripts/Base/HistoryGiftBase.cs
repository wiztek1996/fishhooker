﻿public class HistoryGiftBase {
    public int ID { get; set; }
    public string Name { get; set; }
    public string Status { get; set; }
    public string TimeRequest { get; set; }
    public string TimeProcess { get; set; }

    public HistoryGiftBase(int id,string name, string status, string timeRequest, string timeProcess)
    {
        this.ID = id;
        this.Name = name;
        this.Status = status;
        this.TimeRequest = timeRequest;
        this.TimeProcess = timeProcess;        
    }
}
