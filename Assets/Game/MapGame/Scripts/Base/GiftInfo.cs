﻿using UnityEngine;

public class GiftInfo
{
    public int Id { get; set; }
    public int Type { get; set; }
    public string Name { get; set; }
    public long Cost { get; set; }
    public string Telco { get; set; }
    public long Price { get; set; }
    public string Des { get; set; }
    public string Links { get; set; }
    public long Balance { get; set; }
    public Sprite sprite;

    public GiftInfo(int id, int type, string name, long cost, string telco, long price, string des, string links, long balance)
    {
        this.Id = id;
        this.Type = type;
        this.Name = name;
        this.Cost = cost;
        this.Telco = telco;
        this.Price = price;
        this.Des = des;
        this.Links = links;
        this.Balance = balance;
    }
    //public override string ToString()
    //{
    //    return "ID: " + Id + " Type: " + Type + " Name: " + Name + " Cost: " + Cost + " Telco: " + Telco + " Price: " + Price + " Des: " + Des + " Links: " + Links + " Balance: " + Balance;
    //}
}
