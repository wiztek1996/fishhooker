﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HistoryTransaction {

    public long moneyOld { get; set; }
    public long moneyNew { get; set; }
    public long moneyWin { get; set; }
    public string description { get; set; }
    public string time { get; set; }

    public HistoryTransaction(long moneyOld, long moneyNew, long moneyWin, string description, string time)
    {
        this.moneyOld = moneyOld;
        this.moneyNew = moneyNew;
        this.moneyWin = moneyWin;
        this.description = description;
        this.time = time;
    }
}
