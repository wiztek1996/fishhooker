﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTelcoInfo  {

    public int ID { get; set; }
    private int menhgia = 0;
    private int xu = 0;
    private int khuyenmai = 0;

    public CardTelcoInfo(int id, int menhgia, int xu, int khuyenmai)
    {
        this.ID = id;
        this.menhgia = menhgia;
        this.xu = xu;
        this.khuyenmai = khuyenmai;
    }

    public int getMenhgia()
    {
        return menhgia;
    }

    public int getXu()
    {
        return xu;
    }

    public int getKhuyenmai()
    {
        return khuyenmai;
    }
}
