﻿using UnityEngine;

public class Sound : MonoBehaviour
{

    public static Sound Instance;
    public static bool isMusic = true;
    public AudioSource aS;
    public AudioClip[] aClip;

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        if (PlayerPrefs.GetInt(C.PP_MUSIC, 1) == 1)
        {
            isMusic = true;
        }
        else { isMusic = false; }
    }

    public void PlayMusicGame(int music)
    {
        if (isMusic)
        {
            aS.PlayOneShot(aClip[music]);
        }
    }
    public void PlayRandom()
    {
        int ran = Random.Range(1, 4);
        int audio = 0;
        switch (ran)
        {
            case 1:
                audio = Audio.DO_DI;
                break;
            case 2:
                audio = Audio.MAY_HA_BUOI;
                break;
            case 3:
                audio = Audio.THUA_DI_CUNG;
                break;
            case 4:
                audio = Audio.CHET_NE;
                break;

        }
        PlayMusicGame(audio);
    }
    /// <summary>
    /// Tắt âm thanh đang phát
    /// </summary>
    public void StopAudio()
    {
        aS.Stop();
    }
}
