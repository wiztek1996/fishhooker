﻿public static class Audio
{
    public const int DANH_BAI = 0;
    public const int VAO_BAN = 1;
    public const int DAT_CUOC = 2;
    public const int MESSAGE = 3;
    public const int COUNT_DOWN_TIME = 4;
    public const int TIME_COUNT = 5;
    public const int TO = 6;
    public const int WIN = 7;
    public const int ADD = 8;
    public const int THUA_DI_CUNG = 9;
    public const int DO_DI = 10;
    public const int MAY_HA_BUOI = 11;
    public const int HAI_NE = 12;
    public const int CHET_NE = 13;
    public const int BET = 14;
    public const int MOM = 15;
    public const int BA = 16;
    public const int U = 17;
    public const int XOC_DIA = 18;
    public const int AN_BAI = 19;
    public const int HA_PHOM = 20;
    public const int GUI_BAI = 21;
    public const int THEO = 22;
    public const int CUOC_NHIEU_XU = 23;
    public const int CLICK = 24;
    public const int MONEY = 25;
    public const int THOI_GIAN_CHO = 26;
    public const int FINISH = 27;
    public const int BINH_LUNG = 28;
    public const int MAU_THAU = 29;
    public const int DOI = 30;
    public const int THU = 31;
    public const int SAM_CO = 32;
    public const int SANH = 33;
    public const int THUNG = 34;
    public const int CU_LU = 35;
    public const int TU_QUY = 36;
    public const int THUNG_PHA_SANH = 37;
    public const int MAU_BINH = 38;
    public const int XENG_WIN = 39;
    public const int XENG_LOSE = 40;
    public const int XENG_SPIN = 41;
    public const int XENG_MONEY = 42;

    // Slot
    public const int CHON_QUAY = 43;
    public const int QUAY_SLOT = 44;
}
