﻿public class MainInfo
{
    public string NickName { get; set; }
    public long UserID { get; set; }
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
    public long Money { get; set; }// Tien Vip

    public string DisplayName { get; set; }
    public sbyte IsVIP { get; set; }
    // ----- new
    public string SoLanThang { get; set; }
    public string SoLanThua { get; set; }
    public long SoTienMax { get; set; }
    public int SoGDThanhCong { get; set; }
    public bool IsAutoOutTable { get; set; }
    /**
	 * check xem người chơi có đang chơi trong ván ko 
	 */
    public bool IsPlayingUser { get; set; }

    public long Exp { get; set; }
    public long Score_vip { get; set; }
    public long Total_money_charging { get; set; }
    public long Total_time_play { get; set; }

    public string Link_Avatar { get; set; }
    public int IdAvatar { get; set; }
    public int Vp { get; set; }
    public int Vp_next { get; set; }
    public int Level_vip { get; set; }
    public string email_address { get; set; }
    public bool isAgency { get; set; }
    
    public sbyte GioiTinh
    {
        get; set;
    }
}
