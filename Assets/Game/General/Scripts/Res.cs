﻿public class Res
{
    public static string VERSION = "1.3.4";
    //public static string IP = "123.31.45.20";
    //public static int PORT = 4322;
    /*public const string IP = "vking.choibaidoithe.com";*/
#if NETFX_CORE || WINDOWS_PHONE
    public const string IP = "wphone.choibaidoithe.com";//using WINDOW PHONE
#else
    public static string IP = "";
    //public static string IP = "";
    //public static string IP = "localhost";
#endif
    public static int PORT = 6468;
    public static string IPGET = "http://api.bancavip68.com/api/ip?provider=";
    public static string LINK_GAME = "";
    public const int ROOMFREE = 1;
    public const int ROOMVIP = 2;

    public const string APP_ID_FACEBOOK = "278505019538538";

    public static string moreGameLink = "";
    public static string linkVote = "";
    public static string linkGame = "";
    public static string linkForum = "https://www.facebook.com/B%E1%BA%AFn-C%C3%A1-Vip-348240265760896";
    public static string TXT_PhoneNumber = "";
    public static string TXT_PhoneNumber2 = "https://www.facebook.com/B%E1%BA%AFn-C%C3%A1-Vip-348240265760896";
    public static string TXT_EmailDoiThuong = "";
    public static string TXT_EmailnapTien = "";
    public static string TXT_Fanpage = "https://www.facebook.com/B%E1%BA%AFn-C%C3%A1-Vip-348240265760896";
    public static string TXT_ID_Fanpage = "195052420972899";
    public static string TXT_Noti = "Chào Mừng Bạn Đến Với Slot Bắn Cá Vip 2019";
    public static string LINK_COIN = "";

    public const string TITLE_THONG_BAO = "THÔNG BÁO";
    public const string BTN_DONG_Y = "Đồng ý";
    public const string BTN_HUY = "Hủy";
    public const string BTN_BAT_DAU = "Bắt đầu";
    public const string TAIGUI = "Tái gửi";
    public const string KHONGTAIGUI = "Không tái gửi";

    //DoiPass
    public static string SMS_DOI_PASS = "";
    public static string DAU_SO_DOI_PASS = "";

    //ACTIVE
    public static string NOTI_ACTIVE = "";
    public static string SMS_ACTIVE = "";
    public static string PHONE_ACTIVE = "";

    //Reg warning
    public static string USERNAME = "";
    public static string PASSWORD = "";
    public static string IMEI = "";

    //Provision
    public static string LINK_PROVISION = "";
    public static string WARNING_PROVISION = "Vui lòng đồng ý điều khoản trước khi chơi. Xin Cảm ơn!";
    public static int TYPE_LOGIN = 0;
    public const string TIEN_VIP1 = " Vàng";
    public const string TIEN_VIP2 = " Vàng";
    public const int TIEN_VIP_ID = 2;
    public const int TIEN_FREE_ID = 1;

    public const float SPEED_CARD = 0.25f;
    public const int MAX_WIDTH_HA_PHOM = 50;
    public const int MAX_HEIGHT_HA_PHOM = 70;

    public const int MAX_WIDTH_MAU_BINH = 60;
    public const int MAX_HEIGHT_MAU_BINH = 80;

    public const int MAX_WIDTH_CARD_PLAYER_0 = 75;
    public const int MAX_HEIGHT_CARD_PLAYER_0 = 100;

    public const int MAX_WIDTH_CARD_FIRED = 61;

    public const string LOGIN_0 = "Vui lòng đồng ý điều khoản trước khi chơi. Xin cảm ơn.";
    public const string LOGIN_1 = "Bạn chưa nhập tài khoản hoặc mật khẩu.";
    public const string LOGIN_2 = "Lỗi đăng nhập, tài khoản hoặc mật khẩu không đúng.";
    public const string LOGIN_3 = "Tài khoản phải > 6 ký tự.";
    public const string LOGIN_4 = "Tên tài khoản chứa ký tự không hợp lệ. Tài khoản không được chứa ký tự đặc biệt.";

    public const string REGISTER_1 = "Mật khẩu không hợp lệ. Mật khẩu phải > 6 ký tự và có cả chữ và số.";
    public const string REGISTER_2 = "Mật khẩu phải > 6 ký tự, bao gồm cả chữ và số.";
    public const string REGISTER_3 = "Hai mật khẩu không khớp nhau. ";
    public const string REGISTER_4 = "Tên tài khoản chứa ký tự không hợp lệ.";
    public const string REGISTER_5 = "Đăng ký tài khoản thất bại, tài khoản đã có người sử dụng.";

    public const string CHAT_ADMIN = "Bạn phải nhập nội dung.";
    public const string CHANGE_NAME_0 = "Nhập vào tên mới.";
    public const string CHANGE_NAME_1 = "Tên không được chứa ký tự đặc biệt.";
    public const string CHANGE_NAME_2 = "Tên phải nhiều hơn 4 và ít hơn 20.";
    public const string CHANGE_NAME_3 = "Bạn chỉ được đổi tên hiển thị một lần.";

    public const string CHANGE_PASSWORD_0 = "Bạn hãy nhập đủ thông tin.";
    public const string CHANGE_PASSWORD_1 = "Hai mật khẩu mới không khớp.";

    public const string NAP_CARD = "Mã thẻ hoặc Mã seri không hợp lệ.";

    public const string CHUYEN_XU_0 = "Số Xu chuyển phải lớn hơn 500K.";
    public const string CHUYEN_XU_1 = "Bạn phải nạp tiền ít nhất một lần mới có thể chuyển Xu.";
    public const string CHUYEN_XU_2 = "Bạn chưa nhập UserId người nhận.";
    public const string CHUYEN_XU_3 = "Xu của bạn không đủ để chuyển.";

    public const string AVATAR = "Cập nhật avatar thành công.";

    public const string TAO_BAN_0 = "Số người phải lớn hơn 1 và nhỏ hơn 5";

    public const string JOIN_ROOM = "Bạn không còn tiền. bạn có muốn nạp tiền không?";
    public const string JOIN_ROOM_1 = "Bàn đang chơi, xin chờ.";
    public const string JOIN_ROOM_2 = "Bạn không đủ tiền để chơi bàn cược này.";

    public const string TOI_BAN = "Bạn chưa nhập tên bàn.";

    public const string CHON_BAI = "Chưa chọn bài!";
    public const string ROI_BAN = "Bạn đã đăng ký rời bàn thành công !";

    public const string TX_1 = "Không được đặt cả hai cửa trong 1 ván.";
    public const string TX_2 = "Vui lòng chọn mức cược > 0";
    public const string TX_3 = "Bạn không đủ tiền để tham gia đặt cược";
    public const string TX_4 = "Không có dữ liệu phiên.";

    //XiTo
    public const string TXT_FOLD = "Bỏ";
    public const string TXT_CHECK = "Xem";
    public const string TXT_CALL = "Theo";
    public const string TXT_CALL_ANY = "Tố";
    //public const string TXT_RAISE = "Tố";
    public const string TXT_ALLIN = "Tất tay";

    public const int AC_XEMBAI = 0;
    public const int AC_BOLUOT = 1;
    public const int AC_THEO = 2;
    public const int AC_UPBO = 3;
    public const int AC_TO = 4;

    public const string BC_CHON_BAI = "Chọn 1 trong 2 quân bài để mở: ";
    //Ba Cay
    public const string BC_XIN_CHO = "Xin chờ";
    public const string BC_DAT_CUOC = "Đặt cược";
    public const string BC_NAN_BAI = "Nặn bài";
    //Xoc Dia
    public const string XD_NHA_CAI_XOC = "Nhà cái bắt đầu xóc";
    public const string XD_DAT_CUOC = "Đặt cược";
    public const string XD_NHA_CAI_DUNG_CUOC = "Nhà cái dừng cược";
    public const string XD_HET_TIME_DAT_CUOC = "Chưa đến thời gian đặt cược !";
    public const string XD_LAM_CAI = "Cần ít nhất 2 người chơi để làm cái.";
    public const string XD_NGUNG_NHAN_CUOC = "Nhà Cái Ngừng Nhận Cược";
    public const string XD_CHO_VAN_MOI = "Chờ bắt đầu ván mới";
    public const string XD_MO_BAT = "Mở Bát";
    public const string XD_HUY_CUA_CHAN = "Nhà cái hủy của chẵn";
    public const string XD_HUY_CUA_LE = "Nhà cái hủy của lẻ";
}
